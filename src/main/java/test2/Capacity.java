package test2;

@SuppressWarnings("unchecked")
public abstract class Capacity<T extends Number> implements Pourable<T> {

    protected T size;

    protected T milliliters;

    protected boolean isLong;

    protected boolean isFullCapacity;

    protected Capacity(T size) {
        check(size);
        setSize(size);
    }

    protected Capacity(T size, T milliliters) {
        this(size);
        setMilliliters(milliliters);
    }

    public T getSize() {
        return size;
    }

    public T getMilliliters() {
        return milliliters;
    }

    public boolean isFullCapacity() {
        return isFullCapacity;
    }

    protected void setMilliliters(T milliliters) {
        validParameters(milliliters);
        this.milliliters = milliliters;
    }

    protected void setSize(T size) {
        validParameters(size);
        this.size = size;
    }

    private void check(T t) {
        if (t instanceof Long
                || t instanceof Integer
                || t instanceof Byte
                || t instanceof Short)
            isLong = true;
        else if (t instanceof Float || t instanceof Double) {
            isLong = false;
        } else
            throw new RuntimeException("Type generic no valid");
    }

    private <K extends Number> void addMilliliters(K milliliters) {
        if (isLong) {
            this.milliliters = (T) Long.valueOf(this.milliliters.longValue() + milliliters.longValue());
        } else {
            this.milliliters = (T) Double.valueOf(this.milliliters.doubleValue() + milliliters.doubleValue());
        }
    }

    private <K extends Number> void subMilliliters(K milliliters) {
        if (isLong) {
            this.milliliters = (T) Long.valueOf(this.milliliters.longValue() - milliliters.longValue());
        } else {
            this.milliliters = (T) Double.valueOf(this.milliliters.doubleValue() - milliliters.doubleValue());
        }
    }

    private <K extends Number> void validAddMilliliters(K milliliters) {
        if (isLong) {
            if (size.longValue() < this.milliliters.longValue() + milliliters.longValue()
                    || milliliters.longValue() < 0) {
                throw new RuntimeException("Milliliters not valid parameter");
            }
        }
        else if (size.doubleValue() < this.milliliters.doubleValue() + milliliters.doubleValue()
                || milliliters.doubleValue() < 0) {
            throw new RuntimeException("Milliliters not valid parameter");
        }
    }

    private <K extends Number> void validSubMilliliters(K milliliters) {
        if (isLong) {
            if (size.longValue() < milliliters.longValue()
                    || milliliters.longValue() < 0) {
                throw new RuntimeException("Milliliters not valid parameter");
            }
        }
        else if (size.doubleValue() < milliliters.doubleValue()
                || milliliters.doubleValue() < 0) {
            throw new RuntimeException("Milliliters not valid parameter");
        }
    }

    @Override
    public <K extends Number> T pourIn(K milliliters) {
        validAddMilliliters(milliliters);
        addMilliliters(milliliters);
        return this.milliliters;
    }

    @Override
    public <K extends Number> T pourOut(K milliliters) {
        validSubMilliliters(milliliters);
        subMilliliters(milliliters);
        return this.milliliters;
    }

    private void validParameters(T t) {
        if (isLong && t.longValue() < 0 || t.doubleValue() < 0)
            throw new RuntimeException("Not valid parameter t");
    }
}
