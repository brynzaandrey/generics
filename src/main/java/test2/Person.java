package test2;

import test2.Capacity;
import test2.Pourer;

public class Person<
        IN extends Capacity<? extends Number>,
        OUT extends Capacity<? extends Number>>
        extends Pourer<IN, OUT> {
}
