package test2;


public class Cup<T extends Number> extends Capacity<T> implements CapacityView<String> {

    public Cup(T size, T milliliters){
        super(size, milliliters);
    }

    public Cup(T size) {
        super(size);
    }

    @Override
    public String print() {
        return "Size = " + size + " Milliliters = " + milliliters;
    }

}