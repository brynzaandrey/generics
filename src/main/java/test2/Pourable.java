package test2;

import java.io.IOException;

public interface Pourable<T> {

    <K extends Number> T pourIn(K milliliters) throws IOException;
    <K extends Number> T pourOut(K milliliters) throws IOException;
}
