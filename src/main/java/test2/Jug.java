package test2;

import java.io.IOException;

public class Jug<T extends Number> extends Capacity<T> implements CapacityView<String> {

    public Jug(T size, T milliliters) {
        super(size, milliliters);
    }

    public Jug(T size) {
        super(size);
    }

    @Override
    public String print() {
        return "Size = " + size + " Milliliters = " + milliliters;
    }
}
