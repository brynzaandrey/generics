package test2;

import test2.Capacity;

public abstract class Pourer
        <IN extends Capacity<? extends Number>,
                OUT extends Capacity<? extends Number>> {

    public <K extends Number> void pourIn(IN in, OUT out, K milliliters) {
        if (out.isLong && out.getMilliliters().longValue() >= milliliters.longValue()
                ||out.getMilliliters().doubleValue() >= milliliters.doubleValue()) {
            in.pourIn(milliliters);
            out.pourOut(milliliters);
        } else
            throw new RuntimeException("Not enough milliliters");
    }

    public void pourIn(IN in, OUT out) {
        in.pourIn(out.getMilliliters());
        out.pourOut(out.getMilliliters());
    }

    public <K extends Number> void pourOut(IN in, OUT out, K milliliters) {
        if (in.isLong && in.getMilliliters().longValue() >= milliliters.longValue()
                || in.getMilliliters().doubleValue() >= milliliters.doubleValue()
        ) {
            in.pourOut(milliliters);
            out.pourIn(milliliters);
        } else
            throw new RuntimeException("Not enough milliliters");
    }

    public void pourOut(IN in, OUT out) {
        out.pourIn(in.getMilliliters());
        in.pourOut(in.getMilliliters());
    }

}
