import test2.*;

import java.io.IOException;

public class App1 {
    public static void main(String[] args){
//        Capacity<Byte> cup = new Cup<>(Byte.MAX_VALUE, (byte)20);
//        Capacity<Long> jug = new Jug<>(700L, 150L);
//        Pourer<Capacity<Byte>, Capacity<Long>> person = new Person<>();
        Capacity<Double> cup = new Cup<>(300., 100.);
        Capacity<Float> jug = new Jug<>(700F, 150F);
        Pourer<Capacity<Double>, Capacity<Float>> person = new Person<>();
        person.pourIn(cup, jug);
        //person.pourOut(cup, jug, 70F);
        System.out.println("Cup: " + cup.getMilliliters());
        System.out.println("Jug: " + jug.getMilliliters());

    }
}
